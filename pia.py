import re
import random
from telegram.ext import BaseFilter

white_list = ['wolf',
              'horo',
              'holo',
              'sherlock holo',
              'KenOokamiHoro',
              'sherlock-holo',
              'sherlockholo',
              'ホロ']

def pia(text):
    pia_object = re.sub('pia\s+', '', text.strip(' ,.;，。；'))
    pia_object = re.sub('pia', '', pia_object)
    if pia_object.lower() in white_list:
        return '不可以噢'
    elif '狼' in pia_object:
        return '不可以噢'

    pia1 = '拍飞' + pia_object
    pia2 = pia_object + '应该被打pp'
    pia3 = pia_object + '不乖（'
    pia4 = 'Pia!<(=ｏ ‵-′)ノ☆  ' + pia_object
    return random.choice((pia1, pia2, pia3, pia4))


class pia_filter(BaseFilter):
    def filter(self, message):
        return 'pia' in message.text
