import requests
import json

with open('config.json', 'r') as f:
    config = json.load(f)

key = config['yd_key']

def yd_translate(text):
    url = "http://fanyi.youdao.com/openapi.do?keyfrom=yd-translate&key={}&type=data&doctype=json&version=1.1&q=".format(key)
    url = url + text
    yd = requests.get(url)
    json_reslut = yd.json()
    # explanation
    explains = json_reslut['basic']['explains']
    explanation = '\n\040\040\040\040'.join(explains)

    # web reference
    web = []
    web_reference = ''
    for w in json_reslut['web']:
        web_tmp = []
        for wv in w['value']:
            web_tmp.append(wv)
        web_tmp.append(w['key'])
        web.append(web_tmp)
    for web_r_tmp in web:
        web_reference += web_r_tmp[-1]
        web_reference += '\n\040\040\040\040'
        for wen_r_v in web_r_tmp[:-2]:
            web_reference += wen_r_v
            web_reference += ', '
        if web_reference[-1] == ' ':
            web_reference = web_reference[:-2]
        web_reference += '\n\040\040\040\040'
    result = '''explanation:
    {}

web reference:
    {}'''.format(explanation, web_reference)
    return result

# debug
#print(yd_translate('尝试'))
