import time

def run_time(fn):
    def inner(*args, **kwargs):
        start_time = time.time()
        r = fn(*args, **kwargs)
        end_time = time.time()
        print('{}: {:.2f}s'.format(fn.__name__, end_time - start_time))
        return r
    return inner
